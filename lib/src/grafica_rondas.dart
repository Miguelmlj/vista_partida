import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;



class DetalleRondas extends StatefulWidget {
  const DetalleRondas({ Key? key }) : super(key: key);

  @override
  _DetalleRondasState createState() => _DetalleRondasState();
}




class _DetalleRondasState extends State<DetalleRondas> {


List<charts.Series<Rondas,String>> _seriesData = [];


  _generateData(){

    var azules = [ //cartas azules
       Rondas( 'Ronda 1',3),
       Rondas( 'Ronda 2',7),
       Rondas('Ronda 3', 10),
    ];

    var verdes = [ //cartas verdes
       Rondas('Ronda 1', 1),
       Rondas('Ronda 2',6),
       Rondas( 'Ronda 3',10),
    ];

    var rosas = [ //cartas rosas
       Rondas('Ronda 1',  2),
       Rondas('Ronda 2', 2),
       Rondas( 'Ronda 3',2),
    ];

    var negras = [ //cartas negras
       Rondas('Ronda 1',  6),
       Rondas('Ronda 2', 6),
       Rondas( 'Ronda 3',7),
    ];

    

    _seriesData.add(
      charts.Series(
        domainFn: (Rondas rondas,_) => rondas.ronda,
        measureFn: (Rondas rondas,_) => rondas.cantidad,
        id: 'azules',
        data: azules,
        fillPatternFn:(_, __) => charts.FillPatternType.solid,
        fillColorFn: (Rondas rondas, _) => charts.ColorUtil.fromDartColor(Color(0xff01579b)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Rondas rondas,_) => rondas.ronda,
        measureFn: (Rondas rondas,_) => rondas.cantidad,
        id: 'verdes',
        data: verdes,
        fillPatternFn:(_, __) => charts.FillPatternType.solid,
        fillColorFn: (Rondas rondas, _) => charts.ColorUtil.fromDartColor(Color(0xff109618)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Rondas rondas,_) => rondas.ronda,
        measureFn: (Rondas rondas,_) => rondas.cantidad,
        id: 'rosas',
        data: rosas,
        fillPatternFn:(_, __) => charts.FillPatternType.solid,
        fillColorFn: (Rondas pollution, _) => charts.ColorUtil.fromDartColor(Color(0xffec407a)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Rondas rondas,_) => rondas.ronda,
        measureFn: (Rondas rondas,_) => rondas.cantidad,
        id: 'negras',
        data: negras,
        fillPatternFn:(_, __) => charts.FillPatternType.solid,
        fillColorFn: (Rondas rondas, _) => charts.ColorUtil.fromDartColor(Color(0xFF616161)),
      ),
    );
    

  }

  @override
  void initState() {
    super.initState();
    _generateData();
    //_seriesPieData = List<charts.Series<Task,String>>();
    
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xFF212121),
            
              title: Text('Rondas de Partida'),
          ),
          body: Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: [
                        Text('Cantidad de tarjetas por ronda',style: TextStyle(fontWeight: FontWeight.bold)),
                        Expanded(
                          child: charts.BarChart(
                            _seriesData,
                            animate: true,
                            barGroupingType: charts.BarGroupingType.grouped,
                            animationDuration: Duration(seconds: 3),
                          )
                        )
                      ],
                    ),
                  ),
                ),
                
                ),
            
        )
        
      
    );
  }
}




class Rondas{
  String ronda;
  int cantidad;

  Rondas(this.ronda,this.cantidad);
}




























































// class GroupedBarChart extends StatelessWidget {
//   final List<charts.Series> seriesList;
//   final bool animate;

//   GroupedBarChart(this.seriesList);

//   factory GroupedBarChart.withSampleData() {
//     return new GroupedBarChart(
//       _createSampleData(),
//       // Disable animations for image tests.
//       //animate: false,
//     );
//   }


//   @override
//   Widget build(BuildContext context) {
//     return new charts.BarChart(
//       seriesList,
//       animate: animate,
//       barGroupingType: charts.BarGroupingType.grouped,
//     );
//   }

//   /// Create series list with multiple series
//   static List<charts.Series<OrdinalSales, String>> _createSampleData() {
//     final desktopSalesData = [
//       new OrdinalSales('2014', 5),
//       new OrdinalSales('2015', 25),
//       new OrdinalSales('2016', 100),
//       new OrdinalSales('2017', 75),
//     ];

//     final tableSalesData = [
//       new OrdinalSales('2014', 25),
//       new OrdinalSales('2015', 50),
//       new OrdinalSales('2016', 10),
//       new OrdinalSales('2017', 20),
//     ];

//     final mobileSalesData = [
//       new OrdinalSales('2014', 10),
//       new OrdinalSales('2015', 15),
//       new OrdinalSales('2016', 50),
//       new OrdinalSales('2017', 45),
//     ];

//     return [
//       new charts.Series<OrdinalSales, String>(
//         id: 'Desktop',
//         domainFn: (OrdinalSales sales, _) => sales.year,
//         measureFn: (OrdinalSales sales, _) => sales.sales,
//         data: desktopSalesData,
//       ),
//       new charts.Series<OrdinalSales, String>(
//         id: 'Tablet',
//         domainFn: (OrdinalSales sales, _) => sales.year,
//         measureFn: (OrdinalSales sales, _) => sales.sales,
//         data: tableSalesData,
//       ),
//       new charts.Series<OrdinalSales, String>(
//         id: 'Mobile',
//         domainFn: (OrdinalSales sales, _) => sales.year,
//         measureFn: (OrdinalSales sales, _) => sales.sales,
//         data: mobileSalesData,
//       ),
//     ];
//   }
// }

// /// Sample ordinal data type.
// class OrdinalSales {
//   final String year;
//   final int sales;

//   OrdinalSales(this.year, this.sales);
// }





















































// class GraficaRondas extends StatefulWidget {
//   @override
//   State<GraficaRondas> createState() => _GraficaRondasState();
// }

// class _GraficaRondasState extends State<GraficaRondas> {
//   late List<GDPData> _chartData;

//   @override
//   void initState() {
//     _chartData = getChartData();
//     super.initState();

    
//   } 

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('datos estadisticos'),
//       ),
//       body: SfCartesianChart(
//         title: ChartTitle(text: 'continent wise gpd - 2021'),
//         legend: Legend(isVisible: true),
//         series: <ChartSeries>[
//         BarSeries<GDPData, String>(
//           name: 'gdp',
//           dataSource: _chartData, 
//           xValueMapper: (GDPData gdp,_)=>gdp.continent, 
//           yValueMapper: (GDPData gdp,_)=>gdp.gdp,
//           // dataLabelSettings: DataLabelSettings(isVisible: true)
//         )
//       ],
//       primaryXAxis: CategoryAxis(),
//       primaryYAxis: NumericAxis(
//         edgeLabelPlacement: EdgeLabelPlacement.shift,
//         numberFormat: NumberFormat.simpleCurrency(decimalDigits: 0),
//         title: AxisTitle(text: 'GDP in billions of U.S Dollars')
//         ),
//       ),
      
//     );
//   }

//   List<GDPData> getChartData(){
//     final List<GDPData> chartData = [
//       GDPData('Oceania', 1600),
//       GDPData('Africa', 2490),
//       GDPData('S America', 2900),
//       GDPData('Europe', 23060),
//       GDPData('N America', 24880),
//       GDPData('Asia', 34390),

//     ];
//     return chartData;
//   }
// }


// class GDPData{
//   GDPData(this.continent, this.gdp);
//   final String continent;
//   final double gdp;
// }