import 'package:flutter/material.dart';
import 'package:v_partida/src/grafica_rondas.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class DetallesPartida extends StatelessWidget {
  const DetallesPartida({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ContenidoPagina(),
      
    );
  }
}

class ContenidoPagina extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF212121),
        title: const Text('Detalles Partida'),
        ),
      body: Container(
        child: DatosPartida(),
      )
    );
  }
}


class DatosPartida extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
  

    return DataTable(
      columns: const <DataColumn>[
        DataColumn(label: Text('Jugador', style: TextStyle(fontWeight: FontWeight.bold),)),
        DataColumn(label: Text('Puntuaje F.', style: TextStyle(fontWeight: FontWeight.bold),)),
        DataColumn(label: Text('Ganador', style: TextStyle(fontWeight: FontWeight.bold),)),
        DataColumn(label: Text('Detalle Rnds', style: TextStyle(fontWeight: FontWeight.bold),)),

      ],
      rows: <DataRow>[
        DataRow(cells: [
          DataCell(Text('Paco')),
          DataCell(Text('73')),
          DataCell(IconButton(disabledColor: Colors.green, icon: FaIcon(FontAwesomeIcons.thumbsUp), onPressed: null)),
          DataCell(
            IconButton(
              icon: FaIcon(FontAwesomeIcons.eye), 
              onPressed: () {
                final route = MaterialPageRoute(
                  builder: (context){
                    return DetalleRondas(); 
                  });

                  Navigator.push(context, route);
              }
          )


            ),
        ]),

        DataRow(cells: [
          DataCell(Text('Pedro')),
          DataCell(Text('45')),
          DataCell(IconButton(icon: FaIcon(FontAwesomeIcons.thumbsDown),onPressed: null,disabledColor: Colors.red,)),
          DataCell(
            IconButton(
              icon: FaIcon(FontAwesomeIcons.eye), 
              onPressed: () {
                final route = MaterialPageRoute(
                  builder: (context){
                    return DetalleRondas(); 
                  });

                  Navigator.push(context, route);
              }
          ),


            ),
        ]),

        DataRow(cells: [
          DataCell(Text('Pablo')),
          DataCell(Text('25')),
          DataCell(IconButton(icon: FaIcon(FontAwesomeIcons.thumbsDown),onPressed: null,disabledColor: Colors.red,)),
          DataCell(
            IconButton(
             
              icon: FaIcon(FontAwesomeIcons.eye), 
              onPressed: () {
              
                final route = MaterialPageRoute(
                  builder: (context){
                    return DetalleRondas(); 
                  });

                  Navigator.push(context, route);

              }
          ),
            
            ),
        ]),
      
      ],

    );
  }
 
}

